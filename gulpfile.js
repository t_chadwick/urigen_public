var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var gulp = require('gulp');

//Optimise JavaScript
var jsFiles = 'script.js',
    jsDest = '';

gulp.task('default', function() {
    return gulp.src(jsFiles)
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest));
});
